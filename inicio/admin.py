from django.contrib import admin
from .models import contacto , noticias , afiliado , archivos , juventud_concepcionista , pais
from django.contrib.auth.models import User

# Register your models here.

@admin.register(contacto)
class AdminContacto(admin.ModelAdmin):
    list_display = ('id',
                    'nombres',
                    'apellidos',
                    'correo',
                    'mensaje',
                    'institucion',
                    'año_egreso',
                    'fecha_peticion',)




@admin.register(noticias)
class AdminNoticias(admin.ModelAdmin):
    list_display = ('id',
                    'titulo',
                    'descripcion',
                    'fecha_publicacion',
                    'imagen',
                    'pais',)



@admin.register(afiliado)
class AdminNoticias(admin.ModelAdmin):
     list_display = ('id',
                    'nombres',
                    'apellidos',
                    'correo',
                    'institucion',
                    'año_egreso',
                    )


@admin.register(archivos)
class AdminArchivos(admin.ModelAdmin):
    list_display = ('id', 'titulo' , 'archivo', )



@admin.register(juventud_concepcionista)
class AdminJuventud(admin.ModelAdmin):
    list_display = ('id', 'titulo' , 'descripcion' , 'fecha_publicacion' , 'imagen','pais')


@admin.register(pais)
class AdminPais(admin.ModelAdmin):
    list_display = ('pais',)


