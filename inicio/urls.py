from django.urls import path 
from . import views 
from django.contrib.auth import views as auth_views 
from .views import (Init, mision_apostolica , quienes_somos , que_hacemos , 
                ser_concepcionista , contacto, login , detalle_evento, home, lista_afiliados,lista_noticias,
                eliminar_noticia, agregar_archivo, listar_archivos, eliminar_archivo, vista_archivos, nuevanoticia, nuevanoticiajoven , eliminar_noticia_joven,
                editar_noticia, editar_noticia_joven, detalle , listaV , listaH , listaR , listaM ,  listaJoven , detalleJoven,
                ubicacion_contacto, pilares, venezuelainfo , HaitiInfo, MexicoInfo , RepDominicanaInfo )

urlpatterns = [
    path('',Init.as_view() , name="init"),
    path('quienes_somos', quienes_somos.as_view(), name="quienes_somos"),
    path('mision_apostolica', mision_apostolica.as_view(), name="mision_apostolica"),
    path('que_hacemos',que_hacemos.as_view(), name="que_hacemos"),
    path('ser_concepcionista', ser_concepcionista.as_view(), name="ser_concepcionista"),
    path('contacto', contacto.as_view(), name="contacto"),
    path('login', login.as_view(), name="login"),

    path('autenticacion', views.autenticacion, name="autenticacion"),
    path('', auth_views.logout, {'next_page':'/'}, name="logout"),

    path('lista_afiliados', lista_afiliados.as_view(), name="lista_afiliados"),
    path('detalle_evento/<int:pk>', detalle_evento.as_view(), name="detalle_evento"),
    path('home', home.as_view(), name="home"),
    path('lista_publicacion', lista_noticias.as_view(), name="lista_publicacion"),
    path('nuevanoticia', nuevanoticia.as_view(), name="nuevanoticia"),
    path('nuevanoticiajoven', nuevanoticiajoven.as_view(), name="nuevanoticiajoven"),
    path('eliminar_noticia/<int:pk>', eliminar_noticia.as_view(), name="eliminar_noticia"),
    path('eliminar_noticia_joven/<int:pk>', eliminar_noticia_joven.as_view(), name="eliminar_noticia_joven"),
    path('editar_noticia/<int:pk>', editar_noticia.as_view(), name="editar_noticia"),
    path('editar_noticia_joven/<int:pk>', editar_noticia_joven.as_view(), name="editar_noticia_joven"),
    path('detalle/<int:pk>', detalle.as_view(), name="detalle"),
    path('detalleJoven/<int:pk>', detalleJoven.as_view(), name="detalleJoven"),
    path('ubicacion_contacto', ubicacion_contacto.as_view(), name="ubicacion_contacto"),
    path('pilares', pilares.as_view(), name="pilares"),

    path('venezuelainfo', venezuelainfo.as_view(), name="venezuelainfo"),
     path('HaitiInfo', HaitiInfo.as_view(), name="HaitiInfo"),
      path('MexicoInfo', MexicoInfo.as_view(), name="MexicoInfo"),
       path('RepDominicanaInfo', RepDominicanaInfo.as_view(), name="RepDominicanaInfo"),




    #LISTAS 
     path('listaV', listaV.as_view(), name="listaV"),
     path('listaH', listaH.as_view(), name="listaH"),
     path('listaR', listaR.as_view(), name="listaR"),
     path('listaM', listaM.as_view(), name="listaM"),
     path('listaJoven', listaJoven.as_view(), name="listaJoven"),
    #path('agregar_archivo', agregar_archivo.as_view(), name="agregar_archivo"),
    #path('listar_archivos', listar_archivos.as_view(), name="listar_archivos"),
    #path('eliminar_archivo/<int:pk>', eliminar_archivo.as_view(), name="eliminar_archivo"),
    path('vista_archivos', vista_archivos.as_view(), name="vista_archivos"),


]