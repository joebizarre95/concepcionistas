from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.views.generic import (TemplateView, CreateView, ListView, DetailView, DeleteView , UpdateView )
from .models import contacto, noticias , afiliado , archivos , juventud_concepcionista 
from .forms import contactoform , noticiasform , archivoform , noticiasjovenform
from django.urls import reverse_lazy
from django.contrib.auth import authenticate, login , logout
from pprint import pprint
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Q

# Create your views here.

class Init(CreateView):
    model = afiliado
    template_name = 'init/index.html'
    form_class = contactoform
    success_url = reverse_lazy('init')

    def get_context_data(self, *args, **kwargs):
        context = super(Init, self).get_context_data(**kwargs)
        context['noticiaxvenezuela'] = noticias.objects.filter(pais=1)[:1]
        context['noticiaxhaiti'] = noticias.objects.filter(pais=2)[:1]
        context['noticiaxmexico'] = noticias.objects.filter(pais=4)[:1]
        context['noticiaxrep'] = noticias.objects.filter(pais=3)[:1]
        context['archivos'] = archivos.objects.order_by('-id')[:4]
        context['juventud'] = juventud_concepcionista.objects.order_by('-id')[:4]
        return context



class ubicacion_contacto(TemplateView):
    template_name = 'init/ubicacion_contacto.html'


class pilares(TemplateView):
    template_name = 'init/pilares.html'


class mision_apostolica(TemplateView):
    template_name = 'init/mision_apostolica.html' 



class quienes_somos(TemplateView):
    template_name = 'init/quienes_somos.html'



class que_hacemos(TemplateView):
    template_name = 'init/que_hacemos.html'



class ser_concepcionista(TemplateView):
    template_name = 'init/ser_concepcionista.html'



class contacto(CreateView):
    model = afiliado
    form_class = contactoform
    template_name = 'init/contacto.html'
    success_url = reverse_lazy('init')


def autenticacion(request):
    username = request.POST['usuario']
    password = request.POST['password']
    user = authenticate(request, username=username, password=password)
    if user is not None:
        return HttpResponseRedirect('/home')
    else:
        return HttpResponseRedirect('/login')


def logout(request):
    logout(request)
    return Redirect('/')



class login(TemplateView):
    template_name = 'init/login.html'


class home(CreateView, LoginRequiredMixin):
    template_name = 'init/home.html'
    model = noticias 
    form_class = noticiasform
    success_url = reverse_lazy('home')

    def get_context_data(self, *args, **kwargs):
        context = super(home, self).get_context_data(**kwargs)
        context['afiliados'] = afiliado.objects.order_by('-id')[:5]
        return context



class nuevanoticia(CreateView, LoginRequiredMixin):
    template_name = 'init/nueva_noticia.html'
    model = noticias 
    form_class = noticiasform
    success_url = reverse_lazy('nuevanoticia')

    def get_context_data(self, *args, **kwargs):
        context = super(nuevanoticia, self).get_context_data(**kwargs)
        context['noticias'] = noticias.objects.order_by('-id')
        return context


class eliminar_noticia(DeleteView):
    model = noticias
    template_name = 'init/eliminar.html'
    success_url = reverse_lazy('nuevanoticia')

    def get_context_data(self, *args, **kwargs):
        context = super(eliminar_noticia, self).get_context_data(**kwargs)
        context['noticias'] = noticias.objects.order_by('-id')
        return context



class editar_noticia(UpdateView):
    template_name = 'init/editar.html'
    model = noticias 
    form_class = noticiasform
    success_url = reverse_lazy('nuevanoticia')

    def get_context_data(self, *args, **kwargs):
        context = super(editar_noticia, self).get_context_data(**kwargs)
        context['noticias'] = noticias.objects.order_by('-id')
        return context


class detalle(DetailView):
    template_name = 'init/detalle.html'
    model = noticias 


class listaV(ListView):
    template_name = 'init/lista.html'
    model = noticias
    queryset = noticias.objects.filter(pais='1')

class listaH(ListView):
    template_name = 'init/lista.html'
    model = noticias
    queryset = noticias.objects.filter(pais='2')

class listaR(ListView):
    template_name = 'init/lista.html'
    model = noticias
    queryset = noticias.objects.filter(pais='3')

class listaM(ListView):
    template_name = 'init/lista.html'
    model = noticias
    queryset = noticias.objects.filter(pais='4')




class nuevanoticiajoven(CreateView, LoginRequiredMixin):
    template_name = 'init/noticias_joven.html'
    model = juventud_concepcionista 
    form_class = noticiasjovenform
    success_url = reverse_lazy('nuevanoticiajoven')

    def get_context_data(self, *args, **kwargs):
        context = super(nuevanoticiajoven, self).get_context_data(**kwargs)
        context['noticias'] = juventud_concepcionista.objects.order_by('-id')
        return context



class eliminar_noticia_joven(DeleteView):
    model = juventud_concepcionista
    template_name = 'init/eliminar.html'
    success_url = reverse_lazy('nuevanoticiajoven')

    def get_context_data(self, *args, **kwargs):
        context = super(eliminar_noticia_joven, self).get_context_data(**kwargs)
        context['noticias'] = juventud_concepcionista.objects.order_by('-id')
        return context


class editar_noticia_joven(UpdateView):
    template_name = 'init/editar.html'
    model = juventud_concepcionista 
    form_class = noticiasjovenform
    success_url = reverse_lazy('nuevanoticiajoven')

    def get_context_data(self, *args, **kwargs):
        context = super(editar_noticia_joven, self).get_context_data(**kwargs)
        context['noticias'] = juventud_concepcionista.objects.order_by('-id')
        return context


class listaJoven(ListView):
    template_name = 'init/lista_joven.html'
    model = juventud_concepcionista


class detalleJoven(DetailView):
    template_name = 'init/detalle.html'
    model = juventud_concepcionista 



class detalle_evento(DetailView):
    model = noticias 
    template_name = 'init/detalles.html'
    

class lista_afiliados(TemplateView):
    template_name = 'init/lista_afiliado.html'

    def get_context_data(self, *args, **kwargs):
        context = super(lista_afiliados,self).get_context_data(**kwargs)
        context['af'] = afiliado.objects.order_by('-id')
        return context









class lista_noticias(ListView):
    model = noticias 
    template_name = 'init/lista_noticias.html'



class agregar_archivo(CreateView):
    model = archivos
    form_class = archivoform
    template_name = 'init/nuevo_archivo.html'
    success_url = reverse_lazy('lista_publicacion')



class listar_archivos(ListView):
    model = archivos
    template_name = 'init/lista_archivos.html'


class eliminar_archivo(DeleteView):
    model = archivos
    template_name = 'init/eliminar_archivo.html'
    success_url = reverse_lazy('listar_archivos')


class vista_archivos(ListView):
    model = archivos
    template_name = 'init/vista_archivos.html'




class venezuelainfo(TemplateView):
    template_name = 'init/venezuela_info.html'


class HaitiInfo(TemplateView):
    template_name = 'init/haiti_info.html'


class MexicoInfo(TemplateView):
    template_name = 'init/mexico_info.html'


class RepDominicanaInfo(TemplateView):
    template_name = 'init/rep_dominicana.html'