from .models import contacto , noticias , afiliado , archivos , juventud_concepcionista
from django import forms 


class contactoform(forms.ModelForm):
    class Meta:
        model = afiliado 

        fields = ['nombres','apellidos','correo','institucion','año_egreso']
        labels = {'nombres':'nombres',
                    'apellidos':'apellidos',
                    'correo':'correo',
                    'institucion':'institucion',
                    'año de egreso' : 'año_egreso'}

        widgets = {'nombres': forms.TextInput(attrs={'class':'form-control', 'placeholder':'NOMBRES'}),
                    'apellidos': forms.TextInput(attrs={'class':'form-control', 'placeholder':'APELLIDOS'}),
                    'correo': forms.EmailInput(attrs={'class':'form-control', 'placeholder':'CORREO@CORREO.COM'}),
                    'institucion': forms.TextInput(attrs={'class':'form-control', 'placeholder':'INSTITUCION CONCEPCIONISTA'}),
                    'año_egreso': forms.DateInput(attrs={'class':'form-control',}),
                    }


class noticiasform(forms.ModelForm):
    class Meta:
        model = noticias 

        fields = ['titulo','descripcion','imagen', 'pais','fecha_publicacion']
        labels = {'titulo':'titulo',
                    'descripcion':'descripcion',
                    'fecha_publicacion':'fecha_publicacion',
                    'imagen':'imagen',
                    'pais':'pais'}
        widgets = {'titulo': forms.TextInput(attrs={'class':'form-control'}),
                    'descripcion': forms.Textarea(attrs={'class':'form-control'}),
                    'imagen': forms.FileInput(attrs={'class':'form-control'}),
                    'fecha_publicacion': forms.DateInput(attrs={'class':'form-control','type':'hidden'})
                    }



class noticiasjovenform(forms.ModelForm):
    class Meta:
        model = juventud_concepcionista 

        fields = ['titulo','descripcion','imagen', 'pais','fecha_publicacion']
        labels = {'titulo':'titulo',
                    'descripcion':'descripcion',
                    'fecha_publicacion':'fecha_publicacion',
                    'imagen':'imagen',
                    'pais':'pais'}
        widgets = {'titulo': forms.TextInput(attrs={'class':'form-control'}),
                    'descripcion': forms.Textarea(attrs={'class':'form-control'}),
                    'imagen': forms.FileInput(attrs={'class':'form-control'}),
                    'fecha_publicacion': forms.DateInput(attrs={'class':'form-control','type':'hidden'})
                    }



class archivoform(forms.ModelForm):
    class Meta:
        model = archivos

        fields = ['titulo','archivo']

        labels = {'titulo':'titulo', 'archivo':'archivos'}

        widgets = {'titulo': forms.TextInput(attrs={'class':'form-control'}),
                   'archivo': forms.FileInput(attrs={'class':'form-control'}),
                   }
