from django.db import models
from django.utils import timezone

# Create your models here.
class contacto(models.Model):
    nombres = models.CharField(max_length=255, blank=True, null=True )
    apellidos = models.CharField(max_length=255, blank=True, null=True)
    correo = models.CharField(max_length=255, blank=True, null=True)
    mensaje = models.CharField(max_length=255, blank=True, null=True)
    institucion = models.CharField(max_length=255, blank=True, null=True)
    año_egreso = models.DateField(blank=True,default=timezone.now)
    fecha_peticion = models.DateField(blank=True,default=timezone.now)

    def __str__(self):
        return '{}'.format(
            self.nombres,
            self.apellidos,
            self.correo,
            self.mensaje,
            self.institucion,
            self.año_egreso,
            self.fecha_peticion,
        )

    class Meta:
        ordering = ('id',)




class noticias(models.Model):
    titulo = models.CharField(max_length=255, blank=True, null=True)
    descripcion = models.TextField(blank=True)
    fecha_publicacion = models.DateField(blank=True, default=timezone.now)
    imagen = models.ImageField(upload_to='',)
    pais = models.ForeignKey('pais',models.SET_NULL,
                                blank=True,
                                null=True,)

    def __str__(self):
        return '{}'.format(
            self.titulo,
            self.descripcion,
            self.fecha_publicacion,
            self.imagen,
            self.pais,
        )
    
    class Meta:
        ordering = ('id',)
      

    
class afiliado(models.Model):
    nombres = models.CharField(max_length=255, blank=True, null=True )
    apellidos = models.CharField(max_length=255, blank=True, null=True)
    correo = models.CharField(max_length=255, blank=True, null=True)
    institucion = models.CharField(max_length=255, blank=True, null=True)
    año_egreso = models.DateField(blank=True,default=timezone.now)

    def __str__(self):
        return '{}'.format(
            self.nombres,
            self.apellidos,
            self.correo,
            self.institucion,
            self.año_egreso,
        )

    class Meta:
        ordering = ('id',)




class archivos(models.Model):
    titulo = models.CharField(max_length=255, blank=True, null=True)
    archivo = models.FileField(upload_to='',)

    def __str__(self):
        return '{}'.format(
            self.titulo,
            self.archivo
        )

    class Meta:
        ordering = ('id',)



class juventud_concepcionista(models.Model):
    titulo = models.CharField(max_length=255)
    descripcion = models.TextField(blank=True,null=True,)
    fecha_publicacion = models.DateField(blank=True, default=timezone.now)
    imagen = models.ImageField(upload_to='',)
    pais = models.ForeignKey('pais',models.SET_NULL,
                                    blank=True,
                                    null=True,)

    def __str__(self):
        return '{}'.format(
            self.titulo,
            self.descripcion,
            self.fecha_publicacion,
            self.imagen ,
            self.pais,
            )
    class Meta:
        ordering =('id',)


class pais(models.Model):
    pais = models.CharField(max_length=255)

    def __str__(self):
        return '{}'.format(
            self.pais,)

    class Meta:
        ordering = ('id',)